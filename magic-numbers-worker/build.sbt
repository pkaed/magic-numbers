import com.typesafe.sbt.SbtStartScript

seq(SbtStartScript.startScriptForClassesSettings: _*)

net.virtualvoid.sbt.graph.Plugin.graphSettings

/* basic project info */
name := "magic-numbers-worker"

organization := "name.kaeding.magicnumbers"

version := "0.1.0-SNAPSHOT"

description := "worker to grab articles with magic numbers, and store them to the database"

startYear := Some(2013)

/* scala versions and options */
scalaVersion := "2.10.1"

offline := false

scalacOptions ++= Seq("-deprecation", "-unchecked")

javacOptions ++= Seq("-Xlint:unchecked", "-Xlint:deprecation")

/* entry point */
mainClass in (Compile, packageBin) := Some("name.kaeding.magicnumbers.worker.Main")

mainClass in (Compile, run) := Some("name.kaeding.magicnumbers.worker.Main")

/* dependencies */
libraryDependencies ++= Seq (
  "name.kaeding.magicnumbers" %% "magic-numbers-shared" % "0.3.0",
  "ch.qos.logback" % "logback-classic" % "1.0.13",
  "ch.qos.logback" % "logback-core" % "1.0.13",
  "org.clapper" %% "grizzled-slf4j" % "1.0.1",
  "org.scalaz" %% "scalaz-core" % "7.0.0",
  "org.scalaz" %% "scalaz-concurrent" % "7.0.0",
  "org.typelevel" %% "scalaz-contrib-210" % "0.1.4", 
  "org.typelevel" %% "scalaz-nscala-time" % "0.1.4",
  "org.scalacheck" %% "scalacheck" % "1.10.0" % "test",
  "net.databinder.dispatch" %% "dispatch-core" % "0.10.1",
  "org.jsoup" % "jsoup" % "1.7.2",
  "com.chuusai" %% "shapeless" % "1.2.4",
  "org.typelevel" %% "shapeless-scalaz" % "0.1.1"
)

/* you may need these repos */
resolvers ++= Seq(
  "Kaeding.name Releases" at "http://mvn.kaeding.name/releases",
  "Kaeding.name Snapshots" at "http://mvn.kaeding.name/snapshots",
  "Typesafe Releases" at "http://repo.typesafe.com/typesafe/releases/",
  "spray" at "http://repo.spray.io/"
  // Resolvers.sonatypeRepo("snapshots")
  // Resolvers.typesafeIvyRepo("snapshots")
  // Resolvers.typesafeIvyRepo("releases")
  // Resolvers.typesafeRepo("releases")
  // Resolvers.typesafeRepo("snapshots")
  // JavaNet2Repository,
  // JavaNet1Repository
)

/* sbt behavior */
logLevel in compile := Level.Warn

traceLevel := 5

releaseSettings

publishArtifact in Test := false

pomIncludeRepository := { _ => false }

pomExtra := (
  <developers>
    <developer>
      <id>pkaeding</id>
      <name>Patrick Kaeding</name>
      <email>patrick@kaeding.name</email>
      <!-- <url></url> -->
    </developer>
  </developers>
)

// Josh Suereth's step-by-step guide to publishing on sonatype
// httpcom://www.scala-sbt.org/using_sonatype.html

/* assembly plugin */
mainClass in AssemblyKeys.assembly := Some("name.kaeding.magicnumbers.magic-numbers-worker.Main")

assemblySettings

test in AssemblyKeys.assembly := {}
