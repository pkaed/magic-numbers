package name.kaeding.magicnumbers
package worker

import scalaz._, Scalaz._
import shapeless._, HList._
import TypeOperators._
import MapperAux._
import shapeless.contrib.scalaz._
import scalaz.contrib.std.scalaFuture._
import reactivemongo.api._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import mongo._
import model._

object Main extends App with Logging {
  val db = DBHelper.initDb
  info(s"DB info: $db")

  import FeedFetcherTask._
  import NewsSource._

  object FetchScheduler extends Poly1 {
    implicit val caseWSJ = at[WSJSource](FeedFetcherTask.scheduleFetch(db, FeedFetcherTask.WSJFetcher, _))
    implicit val caseBBC = at[BBCSource](FeedFetcherTask.scheduleFetch(db, FeedFetcherTask.RssFetcher, _))
    implicit val caseWP = at[WashingtonPostSource](FeedFetcherTask.scheduleFetch(db, FeedFetcherTask.RssFetcher, _))
    implicit val caseNYT = at[NewYorkTimesSource](FeedFetcherTask.scheduleFetch(db, FeedFetcherTask.RssFetcher, _))
    implicit val caseCNN = at[CNNSource](FeedFetcherTask.scheduleFetch(db, FeedFetcherTask.RssFetcher, _))
    implicit val caseFR = at[FederalRegisterSource](FeedFetcherTask.scheduleFetch(db, FeedFetcherTask.RssFetcher, _))
  }
  info("starting fetchers")
  sources.map(FetchScheduler)

}

object DBHelper extends Logging {
  def initDb: DB = {
    info("initializing database")
    val driver = new MongoDriver
    val mongouri = Option(System.getenv("MONGOLAB_URI")).getOrElse("mongodb://localhost/magicnumbers")
    val (dbName, hosts, auth) = MongoStore.parseURI(mongouri)
    val connection = driver.connection(hosts, auth)
    val db: DB = connection(dbName)
    runAsyncLogError(FetchRecordStore.ensureIndices(db) >> NewsArticleStore.ensureIndices(db))
    db
  }
}
