package name.kaeding.magicnumbers
package worker

import scalaz._, Scalaz._, effect._
import scalaz.concurrent.Actor
import scalaz.contrib.std.scalaFuture._
import shapeless._
import dispatch.{ Future ⇒ _, _ }, Defaults._
import xml.{ Node ⇒ SNode, _ }
import model._
import org.jsoup.Jsoup
import org.jsoup.nodes.{ Document ⇒ SoupDoc }
import scala.util.matching.Regex.Match
import math._
import name.kaeding.magicnumbers.mongo._
import reactivemongo.api.DB
import com.github.nscala_time.time.Imports._
import java.text.{ SimpleDateFormat, ParseException }
import java.util.{ Timer, TimerTask }

trait FeedFetcherTask[A <: NewsSource] extends Logging {
  import FeedFetcherTask._
  def gov: Governor
  def fetch(db: DB)(a: A): Unit
  def filterItems(feed: AsyncResult[Option[Elem]]): AsyncResult[NodeSeq]
  def processItem(source: RssNewsSource, db: DB)(n: SNode): AsyncResult[Option[LinkResult]] = {
    def makeArticle(
      headline: String,
      link: String,
      doc: SoupDoc,
      pubDate: DateTime): Option[NewsArticle.Id ⇒ NewsArticle] =
      for {
        content ← Option(doc.select(source.contentSelector))
        text ← Option(content.text)
        _ = debug(s"found ${text.length} characters in $headline")
        matches ← Option(magicPattern.findAllIn(text).matchData).filterNot(_.isEmpty).map(_.toList)
        _ = info(s"found ${matches.length} match(es) in $link")
      } yield id ⇒ NewsArticle(
        id = id,
        source = source.id,
        headline = headline,
        link = link,
        pubDate = pubDate,
        fetchDate = DateTime.now,
        fullText = text,
        snippets = matches.map(makeSnippet))
    def alreadyVisited(link: String): AsyncResult[Option[Unit]] = FetchRecordStore.alreadyVisited(db)(link).map(_ ? none[Unit] | ().some)
    def fetch(link: String): AsyncResult[Option[String]] = gov.reqAsyncClearance >> Http(url(link) OK as.String)
    (for {
      title ← OptionT(liftAsyncResult((n \ "title").headOption.map(_.text)))
      link ← OptionT(liftAsyncResult((n \ "link").map(_.text).filterNot(_.isEmpty).headOption))
      pubDate ← OptionT(liftAsyncResult((n \ "pubDate").headOption.map(_.text).flatMap(parsePubDate)))
      guid = (n \ "guid").headOption.map(_.text).getOrElse(link)
      _ ← OptionT(alreadyVisited(guid))
      description ← OptionT(liftAsyncResult((n \ "description").headOption.map(_.text)))
      _ = debug(s"fetching article: $link")
      content ← OptionT[AsyncResult, String](fetch(link))
      soup = Jsoup.parse(content)
      _ = debug(s"found ${soup.title} in $link")
    } yield LinkResult(guid, pubDate, makeArticle(title, link, soup, pubDate))).run
  }
}

object FeedFetcherTask extends Logging {
  def scheduleFetch[A <: NewsSource](db: DB, f: FeedFetcherTask[A], a: A) = {
    val timer = new Timer(s"Timer-${a.id}")
    def handleError(e: Throwable): IO[Unit] = IO(error(s"Error executing sheduled task: ${e.getMessage}", e))
    def exexTask(fn: () ⇒ Unit) = IO(fn()).except(handleError).unsafePerformIO
    val actor = Actor((fn: () ⇒ Unit) ⇒ exexTask(() ⇒ fn()))
    val task = new TimerTask {
      def run = actor ! (() ⇒ f.fetch(db)(a))
    }
    timer.schedule(task, 5.seconds.toDuration.getMillis, 15.minutes.toDuration.getMillis)
  }

  def recordLinkResult(db: DB, s: NewsSource.Id)(lr: LinkResult): AsyncResult[Unit] = lr match {
    case LinkResult(l, d, Some(fn)) ⇒
      FetchRecordStore.alreadyVisited(db)(l).
        ifM(().point[AsyncResult],
          (NewsArticleStore.create(db)(fn) >>=
            ((a: NewsArticle) ⇒ FetchRecordStore.record(db)(l, d, s, a.snippets.length))) as ())
    case LinkResult(l, d, None) ⇒
      FetchRecordStore.alreadyVisited(db)(l).
        ifM(().point[AsyncResult],
          FetchRecordStore.record(db)(l, d, s, 0) as ())
  }

  val Http = dispatch.Http.configure(_ setFollowRedirects true)

  def parsePubDate(s: String): Option[DateTime] = {
    val df = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss z")
    try {
      Option(df.parse(s)).map(d ⇒ new DateTime(d.getTime))
    } catch {
      case e: ParseException ⇒ none
    }
  }

  def makeSnippet(m: Match): Snippet =
    Snippet(
      prefix = m.source.subSequence(max(0, m.start - Snippet.contextSize), m.start).toString,
      magicNumber = m.source.subSequence(m.start, m.end).toString,
      suffix = m.source.subSequence(m.end, min(m.source.length(), m.end + Snippet.contextSize)).toString,
      startPos = m.start,
      endPos = m.end)

  val WSJFetcher = new FeedFetcherTask[WSJSource] {
    val gov = new Governor(2000)
    def filterItems(feed: AsyncResult[Option[Elem]]): AsyncResult[NodeSeq] = OptionT(feed).map(_ \\ "item" filter (_.\("category").text == "FREE")).getOrElse(Nil)
    def fetch(db: DB)(f: WSJSource): Unit = {
      runAsyncLogError(f.feedUrls.traverse(fetchFeed(db, f)))
      info(s"done fetching ${f.name}")
    }
    def fetchFeed(db: DB, f: WSJSource)(loc: String): AsyncResult[Unit] = {
      info(s"fetching feed: $loc")
      val feed: AsyncResult[Option[Elem]] = gov.reqAsyncClearance >> Http(url(loc) OK as.xml.Elem)
      val items = filterItems(feed)
      for {
        ns ← items
        lrs ← ns.toList.traverse(processItem(f, db))
        _ ← lrs.flatten.traverse(recordLinkResult(db, f.id))
      } yield ()
    }
  }
  val RssFetcher = new FeedFetcherTask[RssNewsSource] {
    val gov = new Governor(2000)
    def filterItems(feed: AsyncResult[Option[Elem]]): AsyncResult[NodeSeq] = OptionT(feed).map(_ \\ "item").getOrElse(Nil)
    def fetch(db: DB)(f: RssNewsSource): Unit = {
      runAsyncLogError(f.feedUrls.traverse(fetchFeed(db, f)))
      info(s"done fetching ${f.name}")
    }
    def fetchFeed(db: DB, f: RssNewsSource)(loc: String): AsyncResult[Unit] = {
      info(s"fetching feed: $loc")
      val feed: AsyncResult[Option[Elem]] = gov.reqAsyncClearance >> Http(url(loc) OK as.xml.Elem)
      val items = filterItems(feed)
      for {
        ns ← items
        lrs ← ns.toList.traverse(processItem(f, db))
        _ ← lrs.flatten.traverse(recordLinkResult(db, f.id))
      } yield ()
    }
  }
}

case class LinkResult(link: String, pubDate: DateTime, result: Option[NewsArticle.Id ⇒ NewsArticle])