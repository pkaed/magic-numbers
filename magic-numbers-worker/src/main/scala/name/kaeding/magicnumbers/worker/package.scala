package name.kaeding.magicnumbers

import scalaz._, Scalaz._
import dispatch.{ Future ⇒ DFuture, _ }, Defaults._
import scala.concurrent.duration._

package object worker extends Logging {
  implicit def DFuture2AsyncResult[A](df: DFuture[A]): AsyncResult[Option[A]] =
    EitherT(df.either.map(\/.fromEither).map(_.fold(
      l = e ⇒ {
        warn(s"Error processing HTTP Response: ${e.getMessage}", e)
        none.right
      },
      r = some(_).right)))
}