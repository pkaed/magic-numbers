# magic-numbers-worker — worker to grab articles with magic numbers, and store them to the database #

## Build & run ##

```sh
$ cd magic-numbers-worker
$ ./sbt
> run
```

## Contact ##

- Patrick Kaeding
- <a href="patrick@kaeding.name">patrick@kaeding.name</a>
