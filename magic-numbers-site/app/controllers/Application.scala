package controllers

import scalaz._, Scalaz._
import scalaz.contrib.std.scalaFuture._

import play.api._
import play.api.mvc._
import scala.concurrent._
import ExecutionContext.Implicits.global
import play.api.data._
import play.api.data.Forms._

import name.kaeding.magicnumbers.{ AsyncResult ⇒ _, _ }
import model._
import mongo._

// Reactive Mongo imports
import reactivemongo.api._
import play.modules.reactivemongo.MongoController

object Application extends Controller with MongoController {
  type AsyncWebResult[A] = name.kaeding.magicnumbers.AsyncResult[A]

  val pageForm = Form(
    tuple(
      "offset" -> optional(number),
      "size" -> optional(number)))

  def paginate[A](as: DBStream[A])(implicit request: Request[_]): Page[A] = {
    val params = pageForm.bindFromRequest.get
    val offset = params._1.getOrElse(0)
    val size = params._2.getOrElse(10)
    Page(offset, size, as.count, as.as.drop(offset).take(size).toList)
  }

  def about = Action {
    Ok(views.html.about())
  }

  def index = articleList(none)

  def singleSource(source: NewsSource.Id) = articleList(source.some)

  def article(source: NewsSource.Id, id: Long) = AsyncAction { implicit r ⇒
    val oa = NewsArticleStore.findOne(db, id, source)
    oa.map(_.cata(some = a ⇒ Ok(views.html.singleArticle(a)), none = NotFound))
  }

  def fullFeed = articleFeed(none)
  
  def singleSourceFeed(source: NewsSource.Id) = articleFeed(source.some)

  private[this] def articleList(source: Option[NewsSource.Id]) = AsyncAction { implicit r ⇒
    articlesForSource(source).map(as ⇒ Ok(views.html.index(titleForSource(source), as, source)))
  }

  private[this] def articleFeed(source: Option[NewsSource.Id]) = AsyncAction { implicit r ⇒
    articlesForSource(source).map(as ⇒ Ok(views.xml.articleFeed(titleForSource(source), as, source)))
  }

  private[this] def articlesForSource(source: Option[NewsSource.Id])(implicit R: Request[Any]) =
    source.cata(some = NewsArticleStore.findAllForSource(db, _),
      none = NewsArticleStore.findAll(db)).map(paginate)

  private[this] def titleForSource(source: Option[NewsSource.Id]) =
    source.cata(some = s ⇒ s"Recent articles from ${NewsSource.fromKey(s).name}",
      none = "Recent articles from all sources")

  def AsyncAction(a: Request[Any] ⇒ AsyncWebResult[Result]) = Action(r ⇒ Async {
    def handleError(t: Throwable) = {
      println(s"Error processing request: $t.getMessage")
      t.printStackTrace
      InternalServerError("Something went wrong...")
    }
    a(r).run.map(_.fold(
      l = handleError,
      r = identity))
  })

}

case class Page[A](offset: Int, size: Int, total: Int, as: List[A]) {
  def showBack: Boolean = offset =/= 0
  def showNext: Boolean = offset + size < total
  def backParams: String = s"?size=$size&offset=${math.max(0, offset - size)}"
  def nextParams: String = s"?size=$size&offset=${offset + size}"
}