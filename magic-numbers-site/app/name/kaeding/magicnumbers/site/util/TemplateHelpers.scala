package name.kaeding.magicnumbers
package site.util

import model._

import com.github.nscala_time.time.Imports._
import java.text.SimpleDateFormat

object TemplateHelpers {
  def formatForFeed(d: DateTime) = {
    val df = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss z")
    df.format(d.toDate)
  }
  
  val allSources: List[NewsSource] = NewsSource.sources.toList[NewsSource]
  
  val siteTitle = "33 is the magic number..."
    
  val rssUtm = "?utm_source=feed&utm_medium=rss&utm_campaign=feed"
}