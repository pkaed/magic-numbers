require.config({
  paths: {
    "moment": "lib/moment"
  }
});
define(["jquery", "moment"], function($, m) {
    return {
         updateTimes: function() {
    	  $('time').each(function(i, e) {
    	    var time = m($(e).attr('datetime')),
    	        fullTime = time.format("dddd, MMMM Do YYYY, h:mm:ss a Z");

            $(e).html('<span title="' + fullTime + '">' + time.from(m()) + '</span>');
    	  });
        }
    }
});