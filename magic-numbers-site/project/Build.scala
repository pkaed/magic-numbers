import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName         = "magic-numbers-site"
  val appVersion      = "1.0-SNAPSHOT"

  val appDependencies = Seq(
    "name.kaeding.magicnumbers" %% "magic-numbers-shared" % "0.3.0",
    //"ch.qos.logback" % "logback-classic" % "1.0.13",
    "org.clapper" %% "grizzled-slf4j" % "1.0.1",
    "org.scalaz" %% "scalaz-core" % "7.0.0",
    "org.scalaz" %% "scalaz-concurrent" % "7.0.0",
    "org.typelevel" %% "scalaz-contrib-210" % "0.1.4", 
    "org.typelevel" %% "scalaz-nscala-time" % "0.1.4",
    "org.reactivemongo" %% "play2-reactivemongo" % "0.9",
    jdbc,
    anorm
  )


  val main = play.Project(appName, appVersion, appDependencies).settings(
    // Add your own project settings here   
    resolvers ++= Seq("Kaeding.name Releases" at "http://mvn.kaeding.name/releases",
                      "Kaeding.name Snapshots" at "http://mvn.kaeding.name/snapshots"),
    templatesImport += "name.kaeding.magicnumbers._, model._, site.util.TemplateHelpers._",
    requireJs += "listing.js"
  )

}
