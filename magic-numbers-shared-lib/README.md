# Magic Numbers -- Shared Lib — this project can foo a bar! #

## Build & run ##

```sh
$ cd Magic-Numbers----Shared-Lib
$ ./sbt
> run
```

## Contact ##

- Patrick Kaeding
- <a href="patrick@kaeding.name">patrick@kaeding.name</a>
