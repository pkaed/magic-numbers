/* basic project info */
name := "magic-numbers-shared"

organization := "name.kaeding.magicnumbers"

version := "0.4.0-SNAPSHOT"

description := "this project can foo a bar!"

//homepage := Some(url("https://github.com/pkaeding/myproj"))

startYear := Some(2013)

/* scala versions and options */
scalaVersion := "2.10.1"

crossScalaVersions := Seq("2.10.0", "2.10.1")

offline := false

scalacOptions ++= Seq("-deprecation", "-unchecked")

javacOptions ++= Seq("-Xlint:unchecked", "-Xlint:deprecation")

/* entry point */
mainClass in (Compile, packageBin) := Some("name.kaeding.magicnumbers.magic-numbers-shared.Main")

mainClass in (Compile, run) := Some("name.kaeding.magicnumbers.magic-numbers-shared.Main")

/* dependencies */
libraryDependencies ++= Seq (
  "org.clapper" %% "grizzled-slf4j" % "1.0.1",
  "org.reactivemongo" %% "reactivemongo" % "0.9",
  "org.scalaz" %% "scalaz-core" % "7.0.0",
  "org.scalaz" %% "scalaz-concurrent" % "7.0.0",
  "org.typelevel" %% "scalaz-contrib-210" % "0.1.4", 
  "org.typelevel" %% "scalaz-nscala-time" % "0.1.4",
  "com.chuusai" %% "shapeless" % "1.2.4",
  "org.typelevel" %% "shapeless-scalaz" % "0.1.1",
  "org.scalacheck" %% "scalacheck" % "1.10.0" % "test"
)

/* you may need these repos */
resolvers ++= Seq(
  // Resolvers.sonatypeRepo("snapshots")
  // Resolvers.typesafeIvyRepo("snapshots")
  // Resolvers.typesafeIvyRepo("releases")
  // Resolvers.typesafeRepo("releases")
  // Resolvers.typesafeRepo("snapshots")
  // JavaNet2Repository,
  // JavaNet1Repository
)

publishTo <<= version { (v: String) =>
  val base = "/home/pkaeding/mvn.kaeding.name/"
  if (v.trim.endsWith("SNAPSHOT"))
    Some(Resolver.sftp("Maven Snapshots", "spacecompute.com", base + "snapshots" ) as("pkaeding", new File("~/.ssh/id_dsa.pub")))
  else
    Some(Resolver.sftp("Maven Releases", "spacecompute.com", base + "releases" ) as("pkaeding", new File("~/.ssh/id_dsa.pub")))
}

/* sbt behavior */
logLevel in compile := Level.Warn

traceLevel := 5

releaseSettings

publishArtifact in Test := false

pomIncludeRepository := { _ => false }

pomExtra := (
  <developers>
    <developer>
      <id>pkaeding</id>
      <name>Patrick Kaeding</name>
      <email>patrick@kaeding.name</email>
      <!-- <url></url> -->
    </developer>
  </developers>
)

// Josh Suereth's step-by-step guide to publishing on sonatype
// httpcom://www.scala-sbt.org/using_sonatype.html

/* assembly plugin */
mainClass in AssemblyKeys.assembly := Some("name.kaeding.magicnumbers.magic-numbers-shared.Main")

assemblySettings

test in AssemblyKeys.assembly := {}
