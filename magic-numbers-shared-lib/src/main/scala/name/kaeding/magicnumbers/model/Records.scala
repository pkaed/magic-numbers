package name.kaeding.magicnumbers.model

import com.github.nscala_time.time.Imports._

sealed case class FetchRecord(
    id: Long, 
    url: String, 
    source: NewsSource.Id,
    fetchDate: DateTime, 
    pubDate: DateTime,
    numMatches: Int)