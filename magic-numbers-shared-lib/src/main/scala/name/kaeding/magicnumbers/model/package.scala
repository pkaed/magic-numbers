package name.kaeding.magicnumbers

package object model {
  type Tagged[U] = { type Tag = U }
  type @@[T, U] = T with Tagged[U]
}