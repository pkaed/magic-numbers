package name.kaeding.magicnumbers.model

import shapeless._
import shapeless.contrib.scalaz._

sealed trait NewsSource {
  def id: NewsSource.Id
  def name: String
  def homepageUrl: String
}
object NewsSource {
  type Id = String // @@ NewsSource
  object Id {
    def apply(l: String) = l.asInstanceOf[Id]
  }

  val sources = 
    // new FederalRegisterSource :: 
    new CNNSource :: 
    new WSJSource :: 
    new BBCSource :: 
    new WashingtonPostSource :: 
    new NewYorkTimesSource :: 
    HNil

  val fromKey: Map[String, NewsSource] = sources.toList.map(s => s.id -> s).toMap
}

sealed trait RssNewsSource extends NewsSource {
  def id: NewsSource.Id
  def feedUrls: List[String]
  def homepageUrl: String
  def maxFrequency: Int // minutes
  def name: String
  def description: String
  def contentSelector: String
}

class WSJSource extends RssNewsSource {
    val id = "WSJ"
    val feedUrls = List(
      "http://online.wsj.com/xml/rss/3_7014.xml",
      "http://online.wsj.com/xml/rss/3_7085.xml",
      "http://online.wsj.com/xml/rss/3_7031.xml",
      "http://online.wsj.com/xml/rss/3_7455.xml")
    val homepageUrl = "http://online.wsj.com/"
    val maxFrequency = 15
    val name = "The Wall Street Journal"
    val description = ""
    val contentSelector = "#article_story_body"
  }

class BBCSource extends RssNewsSource {
    val id = "BBC"
    val feedUrls = List(
      "http://feeds.bbci.co.uk/news/rss.xml",
      "http://feeds.bbci.co.uk/news/world/rss.xml",
      "http://feeds.bbci.co.uk/news/uk/rss.xml",
      "http://feeds.bbci.co.uk/news/business/rss.xml",
      "http://feeds.bbci.co.uk/news/politics/rss.xml",
      "http://feeds.bbci.co.uk/news/health/rss.xml",
      "http://feeds.bbci.co.uk/news/education/rss.xml",
      "http://feeds.bbci.co.uk/news/science_and_environment/rss.xml",
      "http://feeds.bbci.co.uk/news/technology/rss.xml",
      "http://feeds.bbci.co.uk/news/entertainment_and_arts/rss.xml")
    val homepageUrl = "http://www.bbc.co.uk/news"
    val maxFrequency = 15
    val name = "BBC News"
    val description = ""
    val contentSelector = "#main-content .story-body"
  }

class WashingtonPostSource extends RssNewsSource {
  val id = "WP"
  val feedUrls = List(
      "http://feeds.washingtonpost.com/rss/politics",
      "http://feeds.washingtonpost.com/rss/local",
      "http://feeds.washingtonpost.com/rss/national",
      "http://feeds.washingtonpost.com/rss/world",
      "http://feeds.washingtonpost.com/rss/business",
      "http://feeds.washingtonpost.com/rss/entertainment")
  val homepageUrl = "http://www.washingtonpost.com/"
  val maxFrequency = 15
  val name = "The Washington Post"
  val description = ""
  val contentSelector = "article .entry-content"
}

class NewYorkTimesSource extends RssNewsSource {
  val id = "NYT"
  val feedUrls = List(
	  "http://www.nytimes.com/services/xml/rss/nyt/HomePage.xml",
	  "http://www.nytimes.com/services/xml/rss/nyt/GlobalHome.xml",
	  "http://www.nytimes.com/services/xml/rss/nyt/World.xml",
	  "http://www.nytimes.com/services/xml/rss/nyt/US.xml",
	  "http://www.nytimes.com/services/xml/rss/nyt/NYRegion.xml",
	  "http://feeds.nytimes.com/nyt/rss/Business",
	  "http://feeds.nytimes.com/nyt/rss/Technology",
	  "http://www.nytimes.com/services/xml/rss/nyt/Science.xml",
	  "http://www.nytimes.com/services/xml/rss/nyt/Health.xml")
  val homepageUrl = "http://www.nytimes.com/"
  val maxFrequency = 15
  val name = "The New York Times"
  val description = ""
  val contentSelector = "#content .entry-content"
}

class CNNSource extends RssNewsSource {
  val id = "CNN"
  val feedUrls = List(
	  "http://rss.cnn.com/rss/cnn_topstories.rss",
	  "http://rss.cnn.com/rss/cnn_world.rss",
	  "http://rss.cnn.com/rss/cnn_us.rss",
	  "http://rss.cnn.com/rss/money_latest.rss",
	  "http://rss.cnn.com/rss/cnn_allpolitics.rss",
	  "http://rss.cnn.com/rss/cnn_crime.rss",
	  "http://rss.cnn.com/rss/cnn_tech.rss",
	  "http://rss.cnn.com/rss/cnn_health.rss")
  val homepageUrl = "http://www.cnn.com/"
  val maxFrequency = 15
  val name = "CNN"
  val description = ""
  val contentSelector = "#cnnContentContainer .cnn_strycntntlft p"
}

class FederalRegisterSource extends RssNewsSource {
  val id = "FR"
  val feedUrls = List(
	  "https://www.federalregister.gov/money.rss",
	  "https://www.federalregister.gov/environment.rss",
	  "https://www.federalregister.gov/world.rss",
	  "https://www.federalregister.gov/science-and-technology.rss",
	  "https://www.federalregister.gov/business-and-industry.rss",
	  "https://www.federalregister.gov/health-and-public-welfare.rss")
  val homepageUrl = "https://www.federalregister.gov"
  val maxFrequency = 15
  val name = "The Federal Register"
  val description = ""
  val contentSelector = "#content_area #fulltext_content_area"
}

// Blocked by paywall
class FinancialTimesSource extends RssNewsSource {
  val id = "FT"
  val feedUrls = List(
	  "http://www.ft.com/rss/home/us",
	  "http://www.ft.com/rss/companies",
	  "http://www.ft.com/rss/world",
	  "http://www.ft.com/rss/markets")
  val homepageUrl = "http://www.ft.com"
  val maxFrequency = 15
  val name = "Financial Times"
  val description = ""
  val contentSelector = "#content_area #fulltext_content_area"
}
