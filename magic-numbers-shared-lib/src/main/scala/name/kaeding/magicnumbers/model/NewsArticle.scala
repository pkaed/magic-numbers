package name.kaeding.magicnumbers.model

import com.github.nscala_time.time.Imports._

sealed case class NewsArticle(
    id: NewsArticle.Id,
    headline: String,
    link: String,
    source: NewsSource.Id,
    pubDate: DateTime,
    fetchDate: DateTime,
    fullText: String,
    snippets: List[Snippet])
object NewsArticle {
  type Id = Long //@@ NewsArticle
  object Id {
    def apply(id: Long): Id = id.asInstanceOf[Id]
  }
}

sealed case class Snippet(prefix: String, magicNumber: String, suffix: String, startPos: Int, endPos: Int)
object Snippet {
  val contextSize = 100
}
