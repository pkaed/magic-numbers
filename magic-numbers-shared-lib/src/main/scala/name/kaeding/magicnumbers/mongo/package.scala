package name.kaeding.magicnumbers

import scalaz._, Scalaz._
import reactivemongo.bson._

package object mongo {

  type DBResult[A] = DBError \/ A
  type BSONDocumentHandler[A] = BSONDocumentReader[A] with BSONDocumentWriter[A]
  
  def getValue[T](bson: BSONDocument, name: String)(implicit reader: BSONReader[_ <: BSONValue, T]): T = 
    bson.getAs[T](name).getOrElse(sys.error(s"$name not found"))
}