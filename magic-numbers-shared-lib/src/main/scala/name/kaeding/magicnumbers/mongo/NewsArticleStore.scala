package name.kaeding.magicnumbers
package mongo

import scalaz.{ Index ⇒ _, _ }, Scalaz._
import scalaz.contrib.std.scalaFuture._
import model._
import reactivemongo.api.DB
import reactivemongo.api.indexes._
import reactivemongo.bson._
import reactivemongo.bson.DefaultBSONHandlers._
import com.github.nscala_time.time.Imports._
import scala.concurrent.ExecutionContext

object NewsArticleStore {
  import MongoStore._
  def coll(db: DB) = db("articles")

  def ensureIndices(db: DB)(implicit ec: ExecutionContext): AsyncResult[Unit] =
    EitherT((coll(db).indexesManager.ensure(Index(key = List("link" -> IndexType.Ascending), unique = true, dropDups = true)) >>
      coll(db).indexesManager.ensure(Index(key = List("pubDate" -> IndexType.Descending)))).map(_ ⇒ ().right))

  implicit def NewsSourceBSONDocumentReader: BSONDocumentHandler[NewsArticle] =
    new BSONDocumentReader[NewsArticle] with BSONDocumentWriter[NewsArticle] {
      def read(bson: BSONDocument): NewsArticle =
        NewsArticle(getValue[Long](bson, "_id"),
          getValue[String](bson, "headline"),
          getValue[String](bson, "link"),
          NewsSource.Id(getValue[String](bson, "source")),
          new DateTime(getValue[BSONDateTime](bson, "pubDate").value),
          new DateTime(getValue[BSONDateTime](bson, "fetchDate").value),
          getValue[String](bson, "fullText"),
          getValue[List[Snippet]](bson, "snippets"))
      def write(s: NewsArticle): BSONDocument =
        BSONDocument("_id" -> s.id,
          "headline" -> s.headline,
          "link" -> s.link,
          "source" -> s.source,
          "pubDate" -> BSONDateTime(s.pubDate.getMillis),
          "fetchDate" -> BSONDateTime(s.fetchDate.getMillis),
          "fullText" -> s.fullText,
          "snippets" -> s.snippets)
    }
  implicit def SnippetBSONDocumentReader: BSONDocumentHandler[Snippet] =
    new BSONDocumentReader[Snippet] with BSONDocumentWriter[Snippet] {
      def read(bson: BSONDocument): Snippet =
        Snippet(getValue[String](bson, "prefix"),
          getValue[String](bson, "magicNumber"),
          getValue[String](bson, "suffix"),
          getValue[Int](bson, "startPos"),
          getValue[Int](bson, "endPos"))
      def write(s: Snippet): BSONDocument =
        BSONDocument("prefix" -> s.prefix,
          "magicNumber" -> s.magicNumber,
          "suffix" -> s.suffix,
          "startPos" -> s.startPos,
          "endPos" -> s.endPos)
    }

  def findAll(db: DB)(implicit ec: ExecutionContext): AsyncResult[DBStream[NewsArticle]] =
    MongoStore.findSome[NewsArticle](coll(db), sort = BSONDocument("pubDate" -> -1))

  def create(db: DB)(a: NewsArticle.Id ⇒ NewsArticle)(implicit ec: ExecutionContext): AsyncResult[NewsArticle] =
    MongoStore.create(coll(db), a)

  def findOne(db: DB, id: NewsArticle.Id, source: NewsSource.Id)(implicit ec: ExecutionContext): AsyncResult[Option[NewsArticle]] =
    MongoStore.findOne[NewsArticle](coll(db), BSONDocument("_id" -> id, "source" -> source))

  def findAllForSource(db: DB, source: NewsSource.Id)(implicit ec: ExecutionContext): AsyncResult[DBStream[NewsArticle]] =
    MongoStore.findSome[NewsArticle](coll(db), qb = BSONDocument("source" -> source), sort = BSONDocument("pubDate" -> -1))
}