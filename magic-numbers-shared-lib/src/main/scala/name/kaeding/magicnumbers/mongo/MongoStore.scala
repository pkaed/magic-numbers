package name.kaeding.magicnumbers
package mongo

import reactivemongo.core.commands._
import reactivemongo.api._
import reactivemongo.bson._
import reactivemongo.bson.DefaultBSONHandlers._
import reactivemongo.api.collections.default.BSONCollection
import scala.concurrent._
import scala.concurrent.duration._

import scalaz._, Scalaz._
import scalaz.contrib.std.scalaFuture._

object MongoStore {

  def freshId(c: BSONCollection)(implicit ec: ExecutionContext): AsyncResult[Long] = {
    val idCName = c.name + ".ids"
    val idC = c.db(idCName)
    val query = BSONDocument("name" -> BSONString("nextId"))
    val update = new Update(BSONDocument("$inc" -> BSONDocument("id" -> BSONLong(1))), true)
    val cmd: Command[Option[BSONDocument]] = new FindAndModify(idCName, query, update, upsert = true)
    val foj: Future[Option[Long]] = c.db.command(cmd).map(_.flatMap(_.getAs[Long]("id")))
    val res: Future[DBResult[Long]] = foj.map(_.cata(some = _.right, none = DBError("not found in database").left))

    EitherT(res)
  }

  def findOne[T: BSONDocumentReader](c: BSONCollection, qb: BSONDocument)(implicit ec: ExecutionContext): AsyncResult[Option[T]] = {
    val res: Future[Option[T]] = c.find[BSONDocument](qb).cursor[T].headOption
    EitherT(res.map(_.right))
  }

  def findSome[T: BSONDocumentReader](c: BSONCollection, qb: BSONDocument = BSONDocument(), sort: BSONDocument = BSONDocument("_id" -> 1))(implicit ec: ExecutionContext): AsyncResult[DBStream[T]] = {
    val cur = c.find[BSONDocument](qb).sort(sort).cursor[T]
    val count = cur.nDocs // TODO this doesn't seem to work right
    val res: Future[DBStream[T]] = cur.collect[Stream].map(s => DBStream(s.length, s))
    EitherT(res.map(_.right))
  }

  def create[T: BSONDocumentWriter](c: BSONCollection, f: Long ⇒ T)(implicit ec: ExecutionContext): AsyncResult[T] =
    for {
      id ← freshId(c)
      res = f(id)
      x = EitherT(c.insert[T](res).map(_.right))
    } yield res

  def save[T: BSONDocumentWriter](c: BSONCollection, t: T)(implicit ec: ExecutionContext): AsyncResult[T] = {
    val ins: Future[LastError] = c.insert[T](t, GetLastError())
    EitherT((ins >> t.point[Future]).map(_.right))
  }

  def write[T: BSONDocumentWriter](c: BSONCollection, t: T)(implicit ec: ExecutionContext): Future[LastError] =
    c.insert[T](t, GetLastError())

  def delete(c: Collection, query: BSONDocument)(implicit ec: ExecutionContext): Future[Option[BSONDocument]] = {
    val cmd: Command[Option[BSONDocument]] = new FindAndModify(c.name, query, Remove)
    c.db.command(cmd)
  }

  def update[T: BSONDocumentWriter](c: BSONCollection, qb: BSONDocument, n: T, upsert: Boolean = false)(implicit ec: ExecutionContext): AsyncResult[T] = {
    EitherT(c.update[BSONDocument, T](qb, n, upsert = upsert).map(DBError.fromLastError(_).cata(some = _.left[T], none = n.right)))
  }

  def shutdown(c: MongoConnection)(implicit ec: ExecutionContext): AsyncResult[Unit] = {
    val f: Future[_] = c.askClose()(1.minute)
    EitherT(f.map(_ ⇒ ().right))
  }

  // based on https://github.com/adaptorel/Play-ReactiveMongo/blob/fbad66e984ab5739234fa7c26835f3a0cc03f89f/src/main/scala/play/modules/reactivemongo/ReactiveMongoPlugin.scala#L115
  def parseURI(uri: String): (String, List[String], List[reactivemongo.core.actors.Authenticate]) = {
    val prefix = "mongodb://"
    def parseAuth(usernameAndPassword: String, dbName: String): List[reactivemongo.core.actors.Authenticate] = {
      usernameAndPassword.split(":").toList match {
        case username :: password ⇒ List(reactivemongo.core.actors.Authenticate(dbName, username, password.mkString("")))
        case _                    ⇒ sys.error("Cannot parse mongo uri")
      }
    }
    def parseHostsAndDbName(hostsPortAndDbName: String): (String, List[String]) = {
      hostsPortAndDbName.split("/").toList match {
        case dbNameOnly :: Nil       ⇒ (dbNameOnly, List("localhost"))
        case hostsAndPorts :: dbName ⇒ (dbName.mkString, hostsAndPorts.split(",").foldLeft(List[String]())((coll, hostAndPort) ⇒ hostAndPort :: coll).reverse)
        case _                       ⇒ sys.error("Cannot parse mongo uri")
      }
    }
    val useful = uri.replace(prefix, "")
    useful.split("@").toList match {
      case hostsPortsAndDbName :: Nil            ⇒
        val parsed = parseHostsAndDbName(hostsPortsAndDbName.mkString); (parsed._1, parsed._2, List.empty)
      case usernamePasswd :: hostsPortsAndDbName ⇒
        val parsed = parseHostsAndDbName(hostsPortsAndDbName.mkString); (parsed._1, parsed._2, parseAuth(usernamePasswd, parsed._1))
      case _                                     ⇒ sys.error("Cannot parse mongo uri")
    }
  }
}

case class DBStream[A](count: Int, as: Stream[A])

class DBError(msg: String) extends RuntimeException {
  override def getMessage = msg
}
object DBError {
  def apply(msg: String) = new DBError(msg)
  def fromLastError(e: LastError): Option[DBError] = e.errMsg.map(apply)
}