package name.kaeding.magicnumbers
package mongo

import scalaz.{Index => _, _}, Scalaz._
import scalaz.contrib.std.scalaFuture._
import model._
import reactivemongo.api.DB
import reactivemongo.api.indexes._
import reactivemongo.bson._
import reactivemongo.bson.DefaultBSONHandlers._
import scala.concurrent.ExecutionContext
import com.github.nscala_time.time.Imports._

object FetchRecordStore {
  import MongoStore._
  def coll(db: DB) = db("fetchRecords")
  
  def ensureIndices(db: DB)(implicit ec: ExecutionContext): AsyncResult[Unit] = 
    EitherT(coll(db).indexesManager.ensure(Index(key = List("url" -> IndexType.Ascending), unique = true, dropDups = true)).map(_ => ().right))
  
  implicit def FetchRecordBSONDocumentReader: BSONDocumentHandler[FetchRecord] =
    new BSONDocumentReader[FetchRecord] with BSONDocumentWriter[FetchRecord] {
	  def read(bson: BSONDocument): FetchRecord = 
	    FetchRecord(getValue[Long](bson, "_id"),
	                getValue[String](bson, "url"),
	                getValue[String](bson, "source"),
	                new DateTime(getValue[BSONDateTime](bson, "fetchDate").value),
	                new DateTime(getValue[BSONDateTime](bson, "pubDate").value),
	                getValue[Int](bson, "numMatches"))
	  def write(s: FetchRecord): BSONDocument =
	    BSONDocument("_id" -> s.id,
	    			"url" -> s.url,
	    			"source" -> s.source,
	    			"fetchDate" -> BSONDateTime(s.fetchDate.getMillis),
	    			"pubDate" -> BSONDateTime(s.pubDate.getMillis),
	    			"numMatches" -> s.numMatches)
    }
  
  def record(db: DB)(url: String, pubDate: DateTime, source: NewsSource.Id, numMatches: Int)(implicit ec: ExecutionContext): AsyncResult[FetchRecord] =
    MongoStore.create(coll(db), id => FetchRecord(id, url, source, DateTime.now, pubDate, numMatches))
    
  def alreadyVisited(db: DB)(url: String)(implicit ec: ExecutionContext): AsyncResult[Boolean] =
    MongoStore.findOne(coll(db), BSONDocument("url" -> url)).map(_.isDefined)
}