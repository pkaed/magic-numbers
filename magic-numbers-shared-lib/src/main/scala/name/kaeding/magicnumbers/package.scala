package name.kaeding

import scalaz._, Scalaz._
import scalaz.contrib.std.scalaFuture._
import scala.concurrent._
import scala.concurrent.duration._

package object magicnumbers extends grizzled.slf4j.Logging {
  type Logging = grizzled.slf4j.Logging
  type AsyncResult[+A] = EitherT[Future, Throwable, A]
  def liftAsyncResult[A](a: A): AsyncResult[A] = EitherT(Future.successful(a.right))
  def runAsyncLogError[A](a: AsyncResult[A]): Unit \/ A = Await.result(a.run, Duration.Inf).leftMap(e => {println("Unknown error encountered"); e.printStackTrace})
  val magicPattern = "\\s3[\\.,]?3+[\\.,0]*".r
}